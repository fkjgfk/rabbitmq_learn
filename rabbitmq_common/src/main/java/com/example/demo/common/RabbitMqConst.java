package com.example.demo.common;

/**
 * @author FKJGFK
 * @version 1.0.0
 * @ClassName RabbitMqConst.java
 * @Description RabbitMq常量
 * @createTime 2021/04/09 09:24:48
 */
public class RabbitMqConst {

	public static final String TEST_DIRECT_EXCHANGE = "TestDirectExchange";
	public static final String TEST_DIRECT_QUEUE = "TestDirectQueue";
	public static final String TEST_DIRECT_ROUTING = "TestDirectRouting";

	public static final String TEST_TOPIC_EXCHANGE = "TestTopicExchange";
	public final static String TEST_TOPIC_QUEUE_A = "TestTopicQueueA";
	public final static String TEST_TOPIC_QUEUE_B = "TestTopicQueueB";

	public static final String TEST_TOPIC_ROUTING_A = "topic.a";
	public static final String TEST_TOPIC_ROUTING_B = "topic.b";
	public static final String TEST_TOPIC_ROUTING_C= "topic.#";
}
