package com.example.demo.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.example.demo.common.RabbitMqConst.TEST_DIRECT_QUEUE;
import static com.example.demo.common.RabbitMqConst.TEST_TOPIC_QUEUE_A;

/**
 * @author FKJGFK
 * @version 1.0.0
 * @ClassName TopicReceiver.java
 * @Description 消费者处理
 * @createTime 2021/04/09 13:35:00
 */
@Component
@RabbitListener(queues = TEST_TOPIC_QUEUE_A)
public class TopicReceiver {

	@RabbitHandler
	public void process(String testMsg) {
		System.out.println("TopicReceiver消费者收到消息  : " + testMsg);
	}


}
