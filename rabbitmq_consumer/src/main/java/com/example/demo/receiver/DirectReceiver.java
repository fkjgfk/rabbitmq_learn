package com.example.demo.receiver;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.example.demo.common.RabbitMqConst.TEST_DIRECT_QUEUE;

/**
 * @author FKJGFK
 * @version 1.0.0
 * @ClassName DirectReceiver.java
 * @Description 消费者处理
 * @createTime 2021/04/09 13:35:00
 */
@Component
@RabbitListener(queues = TEST_DIRECT_QUEUE)//监听的队列名称 TestDirectQueue
public class DirectReceiver {

	@RabbitHandler //可以在一个队列接收多种类型消息
	public void process(String testMsg) {
		System.out.println("DirectReceiver消费者收到消息  : " + testMsg);
	}

	/**
	 * 处理完才会接收下一个消息
	 * @param message
	 * @param content
	 * @param channel
	 */
/*	@RabbitListener(queues = TEST_DIRECT_QUEUE)
	public void process(Message message, String content, Channel channel) {
		System.out.println("DirectReceiver消费者收到消息  : " + content);
	}*/

}
