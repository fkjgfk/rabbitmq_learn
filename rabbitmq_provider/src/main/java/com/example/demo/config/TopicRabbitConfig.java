package com.example.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.example.demo.common.RabbitMqConst.*;

/**
 * @author FKJGFK
 * @version 1.0.0
 * @ClassName TopicRabbitConfig.java
 * @Description 主题模式
 * @createTime 2021/04/09 13:39:25
 */
@Configuration
public class TopicRabbitConfig {

	@Bean
	public Queue firstQueue() {
		return new Queue(TEST_TOPIC_QUEUE_A);
	}

	@Bean
	public Queue secondQueue() {
		return new Queue(TEST_TOPIC_QUEUE_B);
	}

	@Bean
	TopicExchange exchange() {
		return new TopicExchange(TEST_TOPIC_EXCHANGE);
	}


	//将firstQueue和testTopicExchange绑定,而且绑定的键值为topic.a
	//这样只要是消息携带的路由键是topic.a,才会分发到该队列
	@Bean
	Binding bindingExchangeMsgA() {
		return BindingBuilder.bind(firstQueue()).to(exchange()).with(TEST_TOPIC_ROUTING_A);
	}

	//将secondQueue和testTopicExchange绑定,而且绑定的键值为用上通配路由键规则topic.#
	// 这样只要是消息携带的路由键是以topic.开头,都会分发到该队列
	@Bean
	Binding bindingExchangeMsgC() {
		return BindingBuilder.bind(secondQueue()).to(exchange()).with(TEST_TOPIC_ROUTING_C);
	}

}